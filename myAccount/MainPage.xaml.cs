﻿using System;
using System.Collections.Generic;
using Npgsql;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace myAccount
{
    public partial class MainPage : ContentPage
    {
        private int monthValue;
        private string yearValue;
        private readonly bool flag = true;


        void YearPicker_SelectedIndexChanged(Object sender, System.EventArgs e)
        {
            yearValue = ((Picker)sender).SelectedItem.ToString();
            if(!flag)
                DownloadData();
        }

        void MonthPicker_SelectedIndexChanged(Object sender, System.EventArgs e)
        {
            monthValue = ((Picker)sender).SelectedIndex + 1;
            if(!flag)
                DownloadData();
        }

        public void FillPickers()
        { 
            Picker monthpicker = this.FindByName<Picker>("monthPicker");
            foreach(string month in App.months)
            {
                monthpicker.Items.Add(month);
            } 
            monthpicker.SelectedIndex = DateTime.Now.Month - 1;
            Picker yearpicker = this.FindByName<Picker>("yearPicker");
            foreach(string year in App.years)
            {
                yearpicker.Items.Add(year);
            }
            yearpicker.SelectedItem = DateTime.Now.Year.ToString();
            return;
        }

        public void DownloadData()
        {
            try
            {
                NpgsqlCommand command = App.Connection.CreateCommand();
                command.CommandText = "select id, to_char(date,'DD/MM/YYYY'), cause, to_char(value, 'FM9990.00') " +
                    "from expense where extract(month from date) = "+monthValue+" and extract(year from date) = "+yearValue+" order by date";
                NpgsqlDataReader reader = command.ExecuteReader();
                StackLayout layout = this.FindByName<StackLayout>("main");
                layout.Children.Clear();
                while (reader.Read())
                {
                    layout.Children.Add(GenerateTable(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString()));
                }
                App.Connection.Close();
                NpgsqlCommand command1 = App.Connection.CreateCommand();
                command1.CommandText = "select to_char(sum(value), 'FM9999990.00') from expense where" +
                    " extract(month from date) = " + monthValue + " and extract(year from date) = " + yearValue;
                reader = command1.ExecuteReader();
                while (reader.Read())
                {
                    this.FindByName<Span>("totalLabel").Text = reader[0].ToString();
                    double data = 0;
                    try
                    {
                        data = Convert.ToDouble(reader[0].ToString());
                    } catch (Exception ex)
                    {
                        ex.ToString();
                    }
                    Color color;
                    if (data < 0)
                    {
                        color = Color.Red;
                    } else
                    {
                        color = new Color(0, 0.85, 0);
                    }
                    this.FindByName<Span>("totalLabel").TextColor = color;
                }                
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                App.Connection.Close();
            }
        }

        void Edit(object id)
        {
            PopupNavigation.Instance.PushAsync(new NewExpense(this, "E", id.ToString())); // modifica spesa
        }

        async void Delete(object id)
        {
            bool answer = await DisplayAlert("Attenzione", "Vuoi eliminare la voce?", "Si", "Annulla");
            if(answer)
            {
                try
                {
                    NpgsqlCommand command = App.Connection.CreateCommand();
                    command.CommandText = "delete from expense where id="+id.ToString();
                    NpgsqlDataReader reader = command.ExecuteReader();
                    App.Connection.Close();
                }
                catch (Exception ex)
                {
                    ex.ToString();

                }
                DownloadData();
            }
        }

        public SwipeView GenerateTable(string id, string date, string cause, string value)
        {
            SwipeItem EditItem = new SwipeItem
            {
                IconImageSource = "pencil.png",
                BackgroundColor = Color.LightGray,
                CommandParameter = id,
                Command = new Command(Edit)
            };
            //favoriteSwipeItem.Invoked += OnFavoriteSwipeItemInvoked;

            SwipeItem DeleteItem = new SwipeItem
            {
                IconImageSource = "white_trash.png",
                BackgroundColor = Color.Red,
                CommandParameter = id,
                Command = new Command(Delete)
            };
            //deleteSwipeItem.Invoked += OnDeleteSwipeItemInvoked;

            SwipeItems left = new SwipeItems(new List<SwipeItem>() { EditItem });
            left.Mode = SwipeMode.Reveal;
            SwipeItems right = new SwipeItems(new List<SwipeItem>() { DeleteItem });
            right.Mode = SwipeMode.Execute;

            Frame frame = new Frame
            {
                CornerRadius = 10,
                HeightRequest = 40,
                HasShadow = false,
                Padding = new Thickness(0,5,0,5)
            };

            Color valColor;
            try
            {
                if (Convert.ToDouble(value) > 0)
                {
                    valColor = new Color(0, 0.85, 0);
                }
                else
                {
                    valColor = Color.Red;
                }
            } catch
            {
                valColor = Color.White;
            }
            Grid grid = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(40) }
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition(),
                    new ColumnDefinition(),
                    new ColumnDefinition()
                },
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            Label labelDate = new Label
            {
                Text = date,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center,
                FontSize = 17,
                Margin = new Thickness(12,0,0,0)
            };
            Label labelCause = new Label
            {
                Text = cause,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center,
                FontSize = 17,
                FontAttributes = FontAttributes.Bold
            };
            Label labelPrice = new Label
            {
                Text = value,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                FontSize = 17,
                TextColor = valColor,
                Margin = new Thickness(0,0,17,0)
            };
            Grid.SetColumn(labelCause, 1);
            Grid.SetColumn(labelPrice, 2);
            grid.Children.Add(labelDate);
            grid.Children.Add(labelCause);
            grid.Children.Add(labelPrice);

            frame.Content = grid;

            SwipeView swipe = new SwipeView
            {
                Content = frame,
                LeftItems = left,
                RightItems = right
            };

            return swipe;
        }

        public MainPage()
        {
            InitializeComponent();
            FillPickers();
            DownloadData();
            flag = false;
        }

        void Insert(object sender, EventArgs args)
        {
            PopupNavigation.Instance.PushAsync(new NewExpense(this, "N")); // nuova spesa
        }
    }
}
