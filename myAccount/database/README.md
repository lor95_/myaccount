### Database
In questa cartella è contenuto il file `db_credentials`.  
Il file contiene __unicamente__ la stringa `<DB_HOST>,<DB_PORT>,<DB_USERNAME>,<DB_USERPW>,<DB_NAME>`.

#### HotFix

In Visual Studio:  
1. tasto dx su `database/db_credentials`;  
2. `Build Action -> EmbeddedResource`;  
3. Compile solution.