﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Npgsql;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace myAccount
{
    public class PriceEntry : Entry
    {
    }
    public class AddCauseViewModel
    {
        public ICommand PressedCommand { set; get; }
        public ICommand FinishPopup { set; get; }
        public AddCauseViewModel(Picker picker)
        {
            PressedCommand = new Command(async () =>
            {
                var popup = new NewCause();
                popup.SetBinding(NewCause.CloseCommandProperty, new Binding("FinishPopup", source: this));
                await PopupNavigation.Instance.PushAsync(popup);
            });

            FinishPopup = new Command((parameter) =>
            {
                picker.Items.Add(parameter.ToString().Replace("'", string.Empty));
                picker.SelectedIndex = picker.Items.Count;
            });
        }
    }

    public partial class NewExpense : Rg.Plugins.Popup.Pages.PopupPage
    {
        bool imageSwitch = false;
        private bool sw = true;
        private readonly MainPage mainPage;
        private readonly string mode;
        private readonly string id_expense;

        public NewExpense(MainPage mainPage, string mode, string id_expense = "-1")
        {
            InitializeComponent();
            this.mainPage = mainPage;
            this.mode = mode;
            this.id_expense = id_expense;
            try
            {
                NpgsqlCommand command = App.Connection.CreateCommand();
                command.CommandText = "select distinct cause from expense order by cause";
                NpgsqlDataReader reader = command.ExecuteReader();
                Picker picker = this.FindByName<Picker>("pickerCause");
                while (reader.Read())
                {
                    picker.Items.Add(reader[0].ToString());
                }
                App.Connection.Close();
                if (mode == "E") // modalità modifica
                {
                    NpgsqlCommand command1 = App.Connection.CreateCommand();
                    command1.CommandText = "select date, cause, value from expense where id=" + id_expense.ToString();
                    reader = command1.ExecuteReader();
                    DatePicker pickerDate = this.FindByName<DatePicker>("pickerDate");
                    PriceEntry price = this.FindByName<PriceEntry>("priceEntry");
                    while (reader.Read())
                    {
                        pickerDate.Date = DateTime.Parse(reader[0].ToString());
                        picker.SelectedIndex = picker.Items.IndexOf(reader[1].ToString());
                        if((double) reader[2] > 0)
                        {
                            imageSwitch = true;
                            ImageButton imgBtn = this.FindByName<ImageButton>("_imageSwitch");
                            imgBtn.Source = "green_plus.png";
                        }
                        sw = false;
                        price.Text = decimal.Parse(reader[2].ToString().Replace("-", string.Empty)).ToString();
                        sw = true;
                    }
                    App.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();

            }
            BindingContext = new AddCauseViewModel(this.FindByName<Picker>("pickerCause"));
        }

        void Switch(object sender, EventArgs args)
        {
            if (imageSwitch)
            {
                ((ImageButton)sender).Source = "red_minus.png";
            }
            else
            {
                ((ImageButton)sender).Source = "green_plus.png";
            }
            imageSwitch = !imageSwitch;
        }

        async void Insert(object sender, EventArgs args)
        {
            this.IsBusy = true;
            await Task.Run(async () => await Task.Delay(1));
            string date = this.FindByName<DatePicker>("pickerDate").Date.ToString("dd/MM/yyyy");
            string cause;
            try
            {
                cause = this.FindByName<Picker>("pickerCause").SelectedItem.ToString();
            }
            catch (Exception)
            {
                this.IsBusy = false;
                await DisplayAlert("Attenzione", "Inserire un motivo.", "OK");
                return;
            }
            double price = 0;
            //if (Device.RuntimePlatform == Device.iOS)
            //{
            price = Convert.ToDouble(this.FindByName<PriceEntry>("priceEntry").Text);
            if (!imageSwitch) price *= -1;
            /*}
            else
            {
                price = Convert.ToDouble(this.FindByName<PriceEntry>("priceEntry").Text.Replace(".", ","));
                if (!imageSwitch) price *= -1;
            }*/
            if (price == 0)
            {
                this.IsBusy = false;
                await DisplayAlert("Attenzione", "Inserire un importo valido.", "OK");
                return;
            }
            try
            {
                NpgsqlCommand command = App.Connection.CreateCommand();
                if (mode == "N") // inserisci nuova spesa
                {
                    command.CommandText = "insert into expense(date,cause,value) values(to_date('" + date + "','dd/MM/yyyy'),'" + cause + "'," + price.ToString().Replace(",", ".") + ");";
                } else // modifica spesa esistente
                {
                    command.CommandText = "update expense set date=to_date('" + date + "','dd/MM/yyyy'), cause='" + cause + "', value=" + price.ToString().Replace(",", ".") + " where id=" + id_expense.ToString();
                }
                command.ExecuteNonQuery();
                App.Connection.Close();
            }
            catch (Exception)
            {
                this.IsBusy = false;
                await DisplayAlert("Attenzione", "Operazione fallita. Riprova più tardi.", "OK");
                return;
            }
            await PopupNavigation.Instance.PopAsync();
            mainPage.DownloadData(); // Aggiorna pagina principale
            this.IsBusy = false;
        }

        void PriceChange(object sender, TextChangedEventArgs e)
        {
            PriceEntry entry = (PriceEntry)sender;
            try
            {
                double text;
                int diff = -1;
                string val;
                try
                {
                    diff = Convert.ToInt16(e.NewTextValue.Replace(e.OldTextValue, string.Empty));
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
                if (sw)
                {
                    sw = false;
                    if (diff != -1)
                    {
                        //if (Device.RuntimePlatform == Device.iOS)
                        //{
                        text = ((Convert.ToDouble(e.OldTextValue) * 1000) + diff) / 100;
                        /*}
                        else
                        {
                            text = ((Convert.ToDouble(e.OldTextValue.Replace(".", ",")) * 1000) + diff) / 100;
                        }*/
                        val = decimal.Parse(Convert.ToString(text)).ToString("N2");
                        entry.Text = val.Replace(",", string.Empty);
                    }
                    else
                    {
                        //if (Device.RuntimePlatform == Device.iOS)
                        //{
                        text = Convert.ToDouble(e.OldTextValue) / 10;
                        /*}
                        else
                        {
                            text = Convert.ToDouble(e.OldTextValue.Replace(".", ",")) / 10;
                        }*/
                        val = decimal.Parse(Convert.ToString(text)).ToString("N3");
                        entry.Text = val.Remove(val.Length - 1).Replace(",", string.Empty);
                    }
                }
                else
                {
                    sw = true;
                }
            }
            catch (Exception)
            {
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // ### Methods for supporting animations in your popup page ###

        // Invoked before an animation appearing
        protected override void OnAppearingAnimationBegin()
        {
            base.OnAppearingAnimationBegin();
        }

        // Invoked after an animation appearing
        protected override void OnAppearingAnimationEnd()
        {
            base.OnAppearingAnimationEnd();
        }

        // Invoked before an animation disappearing
        protected override void OnDisappearingAnimationBegin()
        {
            base.OnDisappearingAnimationBegin();
        }

        // Invoked after an animation disappearing
        protected override void OnDisappearingAnimationEnd()
        {
            base.OnDisappearingAnimationEnd();
        }

        protected override Task OnAppearingAnimationBeginAsync()
        {
            return base.OnAppearingAnimationBeginAsync();
        }

        protected override Task OnAppearingAnimationEndAsync()
        {
            return base.OnAppearingAnimationEndAsync();
        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return base.OnDisappearingAnimationBeginAsync();
        }

        protected override Task OnDisappearingAnimationEndAsync()
        {
            return base.OnDisappearingAnimationEndAsync();
        }

        // ### Overrided methods which can prevent closing a popup page ###

        // Invoked when a hardware back button is pressed
        protected override bool OnBackButtonPressed()
        {
            // Return true if you don't want to close this popup page when a back button is pressed
            return base.OnBackButtonPressed();
        }

        // Invoked when background is clicked
        protected override bool OnBackgroundClicked()
        {
            // Return false if you don't want to close this popup page when a background of the popup page is clicked
            return base.OnBackgroundClicked();
        }
    }
}
