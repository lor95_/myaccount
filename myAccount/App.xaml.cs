﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Reflection;
using Npgsql;
using Xamarin.Forms;

namespace myAccount
{
    public partial class App : Application
    {
        public static string[] months = { "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" };
        public static string[] years = { "2019","2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030" };
        private static NpgsqlConnection _Connection;
        public static NpgsqlConnection Connection
        {
            get
            {
                if (_Connection == null)
                {
                    var assembly = IntrospectionExtensions.GetTypeInfo(typeof(App)).Assembly;
                    Stream stream = assembly.GetManifestResourceStream("myAccount.database.db_credentials");
                    string[] db_conn_params;
                    using (var reader = new StreamReader(stream))
                    {
                        db_conn_params = reader.ReadToEnd().Split(',');
                    }
                    string cs = "Host = " + db_conn_params[0] + "; Port = " + db_conn_params[1] +
                        "; User Id = " + db_conn_params[2] + "; Password = " + db_conn_params[3] + "; Database = " + db_conn_params[4] + ";";
                    _Connection = new NpgsqlConnection(cs);
                }
                if (_Connection.State == ConnectionState.Closed)
                    try
                    {
                        _Connection.Open();
                    }
                    catch (Exception)
                    {
                    }
                return _Connection;
            }
        }

        void SetCulture()
        {
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
        }

        public App()
        {
            SetCulture();
            InitializeComponent();
            MainPage = new MainPage();
        }


        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
