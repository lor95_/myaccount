﻿using System;
using myAccount;
using myAccount.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(PriceField), typeof(PriceEntry))]
namespace myAccount.iOS
{
    public class PriceField : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                var nativeTextField = Control;
                nativeTextField.EditingDidBegin += (object sender, EventArgs eIos) =>
                {
                    nativeTextField.PerformSelector(new ObjCRuntime.Selector("selectAll"), null, 0.0f);
                };
            }
        }

    }
}
