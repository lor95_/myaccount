
CREATE TABLE expense (
  id SERIAL PRIMARY KEY,
  date date NULL,
  cause varchar(200) NULL,
  value double precision NULL
) 