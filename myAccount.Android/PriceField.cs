﻿using Android.Content;
using myAccount;
using myAccount.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(PriceEntry), typeof(PriceField))]
namespace myAccount.Droid
{
    public class PriceField : EntryRenderer
    {
        public PriceField(Context context) : base(context)
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (this.Control != null)
            {
                Control.SetPadding(10, 0, 0, 15);
                Control.InputType = Android.Text.InputTypes.ClassNumber | Android.Text.InputTypes.NumberFlagSigned | Android.Text.InputTypes.NumberFlagDecimal;
                Control.TextAlignment = Android.Views.TextAlignment.Center;
            }
            if (e.OldElement == null)
            {
                var nativeEditText = (global::Android.Widget.EditText)Control;
                nativeEditText.SetSelectAllOnFocus(true);
            }

        }
    }
}
